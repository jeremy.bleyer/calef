#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *

# nombre de haubans de chaque côté
Nh = 15
# geometrie
Lfin = 3
Lstart = 0.5
dL = Lfin - Lstart
H = 1

nm = Node2D([0, 0])
n0 = Node2D([0, H])

cables = []
n_list = []
for k in range(Nh):
    na = Node2D([-Lstart - (k + 1) * dL / float(Nh), 0])
    cables.append(Bar2D([na, n0]))
    n_list.append(na)

    nb = Node2D([Lstart + (k + 1) * dL / float(Nh), 0])
    cables.append(Bar2D([nb, n0]))
    n_list.append(nb)

pylone = [Bar2D([nm, n0], tag="pylone")]
el_list = cables + pylone

mesh = Mesh(el_list)

cable_sect = BeamSection(1.0 / Nh)
cable_mat = Cable(E=1e4)

pyl_sect = BeamSection(S=5)
pyl_mat = LinearElastic(E=210e3)

model = Model(mesh, cable_mat, cable_sect)
model.affect_property(pyl_sect, pylone)
model.affect_property(pyl_mat, pylone)

for el in cables:
    el.sig0 = [0.2]

forces_horz = ExtForce()
forces_horz.add_concentrated_forces(n0, Fx=1)


appuis = Connections()
appuis.add_imposed_displ(n_list, ux=0, uy=0)
appuis.add_imposed_displ(nm, ux=0, uy=0)

K = model.assembl_stiffness_matrix()
L, Ud = model.assembl_connections(appuis)
Fh = model.assembl_external_forces(forces_horz)
F0 = model.assembl_initial_state()

U, lamb = model.solve(K, -F0, L, Ud)

Sig0 = model.stresses(U)

fig = Figure(2, "Normal forces at first increment")
fig.plot(mesh, nodesymbols=False, alpha=0.25)
fig.plot_field_diagrams(mesh, Sig0.get("N"), 0.1)
fig.show()
print(Sig0.get("N").flatten())


# In[ ]:


for e in cables:
    e.internal_var = Sig0.data[e]["N"] / cable_mat.Young_modulus / cable_sect.area
    
Nsteps = 1
Fmax = 2.
Ffin = Fmax
if Ffin == Fmax:
    ampl = np.linspace(0, Fmax, Nsteps + 1)
else:
    ampl = np.hstack(
        (0, np.linspace(0, Fmax, Nsteps + 1), np.linspace(Fmax, Ffin, Nsteps + 1)[1:])
    )


U, lamb, Sig, X, info = model.nonlinear_solver(
    ampl,
    Fh,
    L,
    Ud,
    method="newton-raphson",
    print_info=True,
    nitermax=200,
    tol=1e-6,
)

print("Iterations for each increment\n", [incr_info["niter"] for incr_info in info])
nb_compr = []
for i in range(len(ampl)-1):
    nb_compr.append(sum(Sig[i+1].get("N")==0))
print("Number of elements in compression\n", nb_compr)



fig = Figure(2, "Normal forces at last increment")
fig.plot(mesh, nodesymbols=False, alpha=0.25)
fig.plot_field_diagrams(mesh, Sig[-1].get("N"), 0.1)
fig.show()

fig = Figure(3, "Deformed shape at last increment")
fig.plot_def(mesh, U[:, -1], 400.0, alpha=0.25)
fig.show()

plt.figure()
plt.title("Load-displacement curve", fontsize=16)
plt.plot(U[2, :], ampl, ".-")
plt.xlabel("horizontal displacement of n0")
plt.ylabel("Load factor")
plt.show()


# In[ ]:




