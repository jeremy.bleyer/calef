#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *

# nombre de fibres
N = 6
# dimension de la poutre
h = 1.0
l = 10.0

left = []
right = []
el_list = []
for k in range(N):
    nl = Node2D([0, k / float(N - 1) * h])
    nr = Node2D([l, k / float(N - 1) * h])
    left.append(nl)
    right.append(nr)
    el_list.append(Bar2D([nl, nr]))

mesh = Mesh(el_list)

sect = BeamSection().rect(1, h / float(N))

E = 1e3
fy = 50.0
mat = ElastoPlasticBar(E=E, fy=fy)

model = Model(mesh, mat, sect)

forces = ExtForce()

appuis = Connections()
appuis.add_imposed_displ(left, ux=0, uy=0)
omega = 1.0
appuis.add_imposed_displ(right, ux=[omega * (n.coor[1] - h / 2.0) for n in right], uy=0)


K = model.assembl_stiffness_matrix()
L, Ud = model.assembl_connections(appuis)
F = model.assembl_external_forces(forces)

Nsteps = 1
omega_max = 1.0
omega_fin = omega_max
if omega_fin == omega_max:
    ampl = np.linspace(0, omega_max, Nsteps + 1)
else:
    ampl = np.hstack(
        (
            0,
            np.linspace(0, omega_max, Nsteps + 1),
            np.linspace(omega_max, omega_fin, Nsteps + 1)[1:],
        )
    )
U, lamb, Sig, X, info = model.nonlinear_solver(ampl, F, L, Ud, print_info=True)

Nsteps = len(ampl)
M = np.zeros((Nsteps,))
for i in range(Nsteps):
    M[i] = sum([Sig[i].get("N")[j] * left[j].coor[1] for j in range(N)])

chi = omega / l * ampl
M_p = fy * h ** 2 / 4.0

# I = h**3/12.
# M_el = fy*h**2/6.
# chi_el = M_el/E/I
# plt.plot(chi_el,M_el/M_p,'xk')
# chi_an = np.linspace(chi_el,max(chi_el,max(chi)),100)
# plt.plot([0,chi_el],[0,M_el/M_p],'-k')
# plt.plot(chi_an,1-(chi_el/chi_an)**2/3.,'-k')

plt.plot(chi, M / M_p, ".-")
plt.xlabel(r"Curvature $\omega/l$")
plt.ylabel(r"Normalized bending moment $M/M_p$")
plt.show()


plt.figure()
plt.plot(Sig[-1].get("N") / sect.area, [n.coor[1] - h / 2.0 for n in right], "-o")
plt.xlabel(r"Axial stress $\sigma_{xx}$")
plt.ylabel("y-y0")
plt.plot([-50, -50], [-0.6, 0.6], "--k", linewidth=1)
plt.plot([50, 50], [-0.6, 0.6], "--k", linewidth=1)
plt.show()


# In[ ]:
