#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *

n01 = Node2D([0,-0.1])
n02 = Node2D([0,0.1])
n11 = Node2D([0.2,-0.1])
n12 = Node2D([0.2,0.1])
n21 = Node2D([0.4,-0.1])
n22 = Node2D([0.4,0.1])
n31 = Node2D([0.6,-0.1])
n32 = Node2D([0.6,0.1])
n41 = Node2D([0.8,-0.1])
n42 = Node2D([0.8,0.1])

el01 = Bar2D([n01,n11], tag=2)
el02 = Bar2D([n02,n12])
el03 = Bar2D([n01,n12])
el04 = Bar2D([n02,n11])
el11 = Bar2D([n11,n21], tag=2)
el12 = Bar2D([n12,n22])
el13 = Bar2D([n11,n22])
el14 = Bar2D([n12,n21])
el21 = Bar2D([n21,n31], tag=2)
el22 = Bar2D([n22,n32])
el23 = Bar2D([n21,n32])
el24 = Bar2D([n22,n31])
el31 = Bar2D([n31,n41], tag=2)
el32 = Bar2D([n32,n42])
el33 = Bar2D([n31,n42])
el34 = Bar2D([n32,n41])
el40 = Bar2D([n41,n42])
elv1 = Bar2D([n11, n12])
elv2 = Bar2D([n21, n22])
elv3 = Bar2D([n31, n32])


mesh = Mesh([el01,el02,el03,el04,
             el11,el12,el13,el14,
             el21,el22,el23,el24,
             el31,el32,el33,el34,
             el40, elv1, elv2, elv3])

sect = BeamSection().rect(0.04)
sect2 = BeamSection().rect(0.05)
mat = LinearElastic(200e3)

model = Model(mesh,mat,sect)
model.affect_property(sect2, tag=2)

appuis = Connections()
appuis.add_imposed_displ([n01,n02,n41], ux=[0,0,None], uy=0)

alp = 11e-6
DeltaT = 20.
el01.dilat = alp*DeltaT
el11.dilat = alp*DeltaT
el21.dilat = alp*DeltaT
el31.dilat = alp*DeltaT

fig = Figure(1,title="Mesh")
fig.plot(mesh,nodelabels=True,elemlabels=True)
fig.plot_bc(mesh,appuis)
fig.show()

K = model.assembl_stiffness_matrix()
L, Ud = model.assembl_connections(appuis)

Fther = model.assembl_thermal_strains()

U, lamb = model.solve(K, Fther, L, Ud)

N = model.stresses(U).get("N")

fig = Figure(2,title="Deformed shape")
fig.plot_def(mesh,U,1e3)
fig.show()

fig = Figure(3,title="Normal forces")
fig.plot_field_diagrams(mesh,N,scale=0.4)
fig.show()


# In[ ]:




