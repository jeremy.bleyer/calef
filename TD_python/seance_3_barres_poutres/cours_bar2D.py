#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *

# Définitions des noeuds
n0 = Node2D([0.0, 0.0])  # creation d'un noeud 2D de coordonnées (0,0)
n1 = Node2D([1.0, 0.0])  # un deuxième de coordonnées (1,0)
n2 = Node2D([0.0, -1.0])  # et un troisième

# Définitions des éléments (de type Bar2D)
el0 = Bar2D([n0, n1])  # création d'un élément reliant les noeuds n0 et n1
el1 = Bar2D([n1, n2])  # un deuxième reliant n1 a n2
el2 = Bar2D([n0, n2])  # et un troisième reliant n0 a n2

mesh = Mesh([el0, el1, el2])  # création d'un objet maillage
# regroupant les éléments définis précédemment

# Définition d'un matériau linéaire élastique
mat = LinearElastic(E=1000)  #  module d'Young E = 1000
# Définition d'une section géométrique
sect = BeamSection(S=1.0)  # section S = 1

# Construction d'un modèle (affectation de 'mat' et 'sect' à l'ensemble du maillage)
model = Model(mesh, mat, sect)

# Définition du chargement
forces = ExtForce()  # création d'un objet effort extérieur
forces.add_concentrated_forces(n1, Fy=-1)  # ajout d'une force concentrée (0,-1) en n1

# Définition des conditions aux limites
appuis = Connections()  # création d'un objet Connections (CL, liaisons)
appuis.add_imposed_displ([n0, n2], ux=[0, 0], uy=[0, None])  # déplacement imposé ux=uy=0 en n0 et ux=0 en n2
# Phase d'assemblage
K = model.assembl_stiffness_matrix()  # assemblage de la rigidité
L, Ud = model.assembl_connections(appuis)  # assemblage des liaisons
F = model.assembl_external_forces(forces)  # assemblage des efforts extérieurs

# Résolution du système : déplacement U, multiplicateurs de Lagrange lamb
U, lamb = model.solve(K, F, L, Ud)
Ux = U.get("Ux")  # extraction de la composante suivant x
Uy = U.get("Uy")  # extraction de la composante suivant y


# In[ ]:


# Post-traitement/visualisation
fig = Figure(1, "Mesh")  # création d'une figure
fig.plot(mesh, nodelabels=True, elemlabels=True)  # dessin du maillage et des numéros de noeuds et éléments
fig.show()  # commande d'affichage


# In[ ]:


fig = Figure(2,"Deformed shape")
fig.plot_def(mesh,U,40)   # dessin de la deformée (amplitude x40)
fig.plot_bc(mesh,appuis)  # dessin des conditions aux limites
fig.show()                # commande d'affichage


# In[ ]:


# Validation
print("Displacement node 1",(Ux[1],Uy[1]))
EA = mat.Young_modulus*sect.area
print("Analytic solution",(1./EA,-2*(1+2**0.5)/EA)) # solution analytique


# In[ ]:


# Calcul des contraintes généralisées (effort normal uniquement)
N = model.stresses(U).get("N")
fig = Figure(3, "Normal force")
fig.plot_field_diagrams(mesh, N)  # tracé de l'effort normal
fig.show()


# In[ ]:




