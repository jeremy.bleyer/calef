#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *

H = 10.
gamma = 1.

n0 = Node2D([0, 0])
nfin = Node2D([0, H])

el = Bar2D([n0,nfin])

mesh = Mesh([el])

sect = BeamSection(1.)
mat = LinearElastic(1e3)

model = Model(mesh,mat,sect)

forces = ExtForce()
forces.add_distributed_forces(mesh, fx=, fy=)

appuis = Connections()
appuis.add_imposed_displ( )

fig = Figure(1)
fig.plot(mesh)
fig.plot_bc(mesh,appuis)
fig.show()

K = model.assembl_stiffness_matrix()
L,Ud = model.assembl_connections(appuis)
F = model.assembl_external_forces(forces)
U,lamb = model.solve(K,F,L,Ud)
Ux = U.get("ux")
Uy = U.get("uy")

N = model.stresses(U).get("N")


# In[ ]:


E_pot = 0
print("Potential energy :",E_pot)
print("Displacement :",Uy)
print("Normal force :",N)


# In[ ]:


fig = Figure(2,"Deformed shape")
fig.plot_def(mesh,U,10)
fig.show()


# In[ ]:


fig = Figure(3,"Normal force")
fig.plot_field_diagrams(mesh,N)
fig.show()

