#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *

l = 4.
sect = BeamSection().rect(0.1)

nA = Node2D([0,0])
nB = Node2D([0,l])
nC = Node2D([l,l])

#==============================================================================
## QUESTION 3.4
#
#Nv = 1
#Nh = 10
#na = nA
#poteau = []
#poutre = []
#for i in range(Nv):
#    if i == Nv-1:
#        nb = nB
#    else:
#        nb = Node2D([0,(i+1)*l/Nv])
#    poteau.append(Beam2D([na,nb]))
#    na = nb
#na = nB
#for i in range(Nh):
#    if i == Nh-1:
#        nb = nC
#    else:
#        nb = Node2D([(i+1)*l/Nh,l])
#    poutre.append(Beam2D([na,nb]))
#    na = nb
#mesh = Mesh(poteau + poutre)
#==============================================================================


#================
# QUESTION 3.1
# A COMPLETER

q =
mat = LinearElastic( )

poteau = Beam2D( )
poutre = Beam2D( )
mesh = Mesh([poteau, poutre])


forces = ExtForce()
forces.add_distributed_forces( )

appuis = Connections()
appuis.add_imposed_displ([ ], ux= , uy=, thetaz= )
#================

model = Model(mesh,mat,sect)

fig = Figure(1,"Mesh and boundary conditions")
fig.plot(mesh,elemlabels=True)
fig.plot_bc(mesh,appuis)
fig.show()

K = model.assembl_stiffness_matrix()
L, Ud = model.assembl_connections(appuis)
F = model.assembl_external_forces(forces)
U, lamb = model.solve(K, F, L, Ud)
Ux = U.get("Ux")
Uy = U.get("uy")
Thetaz = U.get("thetaz")

# Extraction des differents efforts interieurs
Sig = model.stresses(U)
N = Sig.get("N")
V = Sig.get("V")
M = Sig.get("M")


fig = Figure(2,"Deformed shape")
fig.plot_def(mesh,U,40)
fig.show(block=False)

fig = Figure(3,"Normal force")
fig.plot_field_diagrams(mesh,N)
fig.show(block=False)
fig = Figure(4,"Shear force")
fig.plot_field_diagrams(mesh,V)
fig.show(block=False)
fig = Figure(5,"Bending moment")
fig.plot_field_diagrams(mesh,M)
fig.show()

## Comparaison solution analytique


# In[ ]:




