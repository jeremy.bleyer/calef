#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *
import os

l = 10.
h = 1.
p = 1.

mesh,boundary = call_gmsh(os.path.join('geometry', 'console.geo'),
                          SolidT3)

bottom = boundary.get_elem_from_tag("bottom")
right = boundary.get_elem_from_tag("right")
top = boundary.get_elem_from_tag("top")
left = boundary.get_elem_from_tag("left")
nF = right.get_nodes()[-1]

#==============================================================================
#
## A COMPLETER
#
mat = LinearElastic(E= ,nu= )

appuis = Connections()

forces = ExtForce()

model = Model(mesh,mat)
#==============================================================================


K = model.assembl_stiffness_matrix()
L, Ud = model.assembl_connections(appuis)
F = model.assembl_external_forces(forces)

U, lamb = model.solve(K, F, L, Ud)

Ux = U.get("ux")
Uy = U.get("uy")


# In[ ]:


fig = Figure(1,"Deformed shape")
fig.plot_def(mesh,U,100)
fig.plot_bc(mesh,appuis)
fig.show()


# In[ ]:


Sigma = model.stresses(U)
Sigxx = Sigma.get("sig_xx")
Sigyy = Sigma.get("sig_yy")
Sigzz = Sigma.get("sig_zz")
Sigxy = Sigma.get("sig_xy")

fig = Figure(3,r"$\sigma_{xx}$ stress")
fig.plot_field(mesh,Sigxx)
fig.show()


# In[ ]:


plt.figure()
plt.spy(K.toarray())
plt.title("Stiffness matrix sparsity pattern")
plt.show()

