#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from wombat import *

n0 = Node2D([0,0])
n1 = Node2D([1,0])
n2 = Node2D([0,2])
n3 = Node2D([1,2])

el1 = SolidT3([n1,n2,n0])
el2 = SolidT3([n2,n1,n3])

mesh = Mesh([el1,el2])

fig = Figure(1)
fig.plot(mesh,nodelabels=True,elemlabels=True)
fig.show(block=False)

print("Coordinates matrix:", mesh.coor)
print("Connectivity matrix:", mesh.connec)

mat = LinearElastic(E=100,nu=0.3)

appuis = Connections()
appuis.add_imposed_displ([n0,n1],uy=0)
appuis.add_imposed_displ([n0,n2],ux=0)
appuis.add_imposed_displ([n2,n3],uy=-0.2)
forces = ExtForce()

model = Model(mesh,mat)

K = model.assembl_stiffness_matrix()
L, Ud = model.assembl_connections(appuis)
F = model.assembl_external_forces(forces)

U, lamb = model.solve(K, F, L, Ud)

Ux = U.get("ux")
Uy = U.get("uy")


# In[ ]:


print("Displacement vector",U)

fig = Figure(2)
fig.plot_def(mesh,U,1.)
fig.show()


# In[ ]:


Sig = model.stresses(U)
print("Stresses",Sig)


# In[ ]:




