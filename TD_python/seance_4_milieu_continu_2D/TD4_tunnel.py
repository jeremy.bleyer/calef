#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *
import os

# Problem parameters
H = 50
R = 3.0
gamma = 2e-3 * 9.81

mesh, boundary = call_gmsh(os.path.join("geometry", "tunnel.geo"), SolidT3)
bottom = boundary.get_elem_from_tag("bottom")
right = boundary.get_elem_from_tag("right")
top = boundary.get_elem_from_tag("top")
left = boundary.get_elem_from_tag("left")


# ==============================================================================
# QUESTION 3.6
# ==============================================================================
soil = mesh.get_elem_from_tag("soil")
reinforcement = mesh.get_elem_from_tag("reinforcement")


mat = LinearElastic(E=500.0, nu=0.3)
mat_concr = LinearElastic(E=70e3, nu=0.2)

appuis = Connections()
appuis.add_imposed_displ(bottom, ux=0, uy=0)
appuis.add_imposed_displ(right, ux=0)
appuis.add_imposed_displ(left, ux=0)


forces = ExtForce()
forces.add_distributed_forces(mesh, fy=-gamma)

for e in mesh.elem_list:
    y_el = e.barycenter()[1]
    e.sig0 = gamma * y_el * np.array([1, 1, 1, 0])

model = Model(mesh, mat)

K = model.assembl_stiffness_matrix()
L, Ud = model.assembl_connections(appuis)
F = model.assembl_external_forces(forces)
Fint0 = model.assembl_initial_state()

U, lamb = model.solve(K, F - Fint0, L, Ud)
Ux = U.get("ux")
Uy = U.get("uy")


# In[ ]:


fig = Figure(1,"Deformed shape")
fig.plot_def(mesh,U,ampl=100.)
fig.show()

fig = Figure(2,"Ux displacement isovalues")
fig.plot_field(mesh,Ux)
fig.show()

fig = Figure(3,"Uy displacement isovalues")
fig.plot_field(mesh,Uy)
fig.show()


# In[ ]:




