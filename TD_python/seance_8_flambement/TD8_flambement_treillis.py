#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *

height = 1.0
cell_length = height
half_length = 10.0
ncell = int(half_length / cell_length)

n0 = Node2D([-cell_length, 0])
n1 = Node2D([0, 0])
n2 = Node2D([0, height])
node_pairs = []
n0c = n0.copy()
node_pairs.append([n0, n0c])
n1c = n1.copy()
node_pairs.append([n1, n1c])
n2c = n2.copy(2)
node_pairs.append([n2, n2c[0]])
node_pairs.append([n2, n2c[1]])
el_list = [Beam2D([n0, n1]), Beam2D([n1c, n2c[0]], tag=2), Beam2D([n2c[1], n0c])]
for k in range(2 * ncell):
    n3 = Node2D([n1.coor[0] + cell_length, 0])
    n3c = n3.copy()
    node_pairs.append([n3, n3c])
    n4 = Node2D([n1.coor[0] + cell_length, height])
    n4c = n4.copy()
    node_pairs.append([n4, n4c])
    el_list.append(Beam2D([n1, n3]))
    el_list.append(Beam2D([n3c, n4c], tag=2))
    el_list.append(Beam2D([n4, n2]))
    n1 = n3
    n2 = n4
    if k == ncell - 1:
        nmid = n1
nfin = Node2D([n1.coor[0] + cell_length, 0])
nfinc = nfin.copy()
node_pairs.append([nfin, nfinc])
n2cc = n2.copy()
node_pairs.append([n2, n2cc])
el_list.append(Beam2D([n1, nfin]))
el_list.append(Beam2D([n2cc, nfinc]))

mesh = Mesh(el_list)

connections = Connections()
for nodes in node_pairs:
    connections.add_relation(nodes[0].get_dof("ux"), nodes[1].get_dof("ux"))
    connections.add_relation(nodes[0].get_dof("uy"), nodes[1].get_dof("uy"))

mat = LinearElastic(70e9, 0, rho=2700.0)
sect = BeamSection().rect(0.1, 0.1)
sect2 = BeamSection().rect(0.05, 0.05)

model = Model(mesh, mat, sect)
model.affect_property(sect2, tag=2)

connections.add_imposed_displ([n0, nfin], ux=0, uy=0)


forces = ExtForce()
forces.add_distributed_forces(mesh, fy=-mat.rho * sect.area * 9.81)

K = model.assembl_stiffness_matrix()
F = model.assembl_external_forces(forces)
L, Ud = model.assembl_connections(connections)
U, reac = model.solve(K, F, L, Ud)

Sig = model.stresses(U)
N = Sig.get("N")
M = Sig.get("M")

fig = Figure(1)
plt.subplot(2, 1, 1)
fig.plot_def(mesh, U, 1.0, undef=True)
plt.subplot(2, 1, 2)
fig.plot_field_diagrams(mesh, N, scale=0.2)
fig.show()

KG = model.assembl_geometric_stiffness_matrix(U)

Nmodes = 4
lmbda, xi = model.eigenmodes(
    K, KG, L, Nmodes, mode="buckling", shift=1e-6, compute_error=True
)


for i in range(Nmodes):
    fig = Figure(2 + i, "Buckling mode " + str(i + 1))
    fig.plot_def(mesh, xi[i], height / max(np.abs(xi[i])), undef=True)

fig.show()


# In[ ]:




