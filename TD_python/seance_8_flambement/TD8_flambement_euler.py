#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *
from math import pi,sqrt,tan
from scipy.optimize import root

# Calcul de la solution exacte
falpha = lambda x: tan(x)-x
alpha = lambda n: root(falpha,0.99*(2*n+1)*pi/2.)['x'][0]

length = 1.
Nel = 1
n0 = Node2D([0,0])
n1 = n0
el_list = []
for k in range(Nel):
    n2 = Node2D([n1.coor[0]+length/Nel,0])
    el_list.append(Beam2D([n1,n2]))
    n1 = n2
nfin = n1

mesh = Mesh(el_list)

sect = BeamSection().rect(0.01)
mat = LinearElastic(70e9,0)

EI = mat.Young_modulus*sect.inertia

model = Model(mesh,mat,sect)

#####################################################################
#
# A COMPLETER
appuis = Connections()
appuis.add_imposed_displ( , ux= ,uy= ,thetaz= )

forces = ExtForce()
forces.add_concentrated_forces(, Fx= )

######################################################################

K = model.assembl_stiffness_matrix()
L, Ud = model.assembl_connections(appuis)
F = model.assembl_external_forces(forces)

U, _ = model.solve(K, F, L, Ud)

KG = model.assembl_geometric_stiffness_matrix(U)

Nmodes = 1
lamb, xi = model.eigenmodes(
    K, KG, L, Nmodes, mode="buckling", compute_error=True, shift=None
)

fig = Figure(2, "Eigenmodes", [Nmodes, 1])
for i in range(Nmodes):
    fcrit_FE = 0
    fcrit_ex = 0
    print("FE critical factor %f Exact critical factor %f" % (fcrit_FE, fcrit_ex))
    plt.subplot(Nmodes, 1, i + 1)
    fig.plot_def(mesh, xi[i], 0.4 / max(np.abs(xi[i])))
    plt.title("Eigenmode " + str(i + 1))
    plt.ylim(-0.25, 0.25)

plt.tight_layout(pad=0)
fig.show()

