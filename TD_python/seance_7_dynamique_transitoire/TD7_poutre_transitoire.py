#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from wombat import *
from math import sqrt

# beam length
length = 1.
# number of elements discretizing the beam
Nel = 1

n0 = Node2D([0,0])
n1 = n0
el_list = []
for k in range(Nel):
    n2 = Node2D([n1.coor[0]+length/Nel,0])
    el_list.append(Beam2D([n1,n2]))
    n1 = n2
nfin = n1

mesh = Mesh(el_list)
sect = BeamSection().rect(0.01,0.01)
mat = LinearElastic(70e9,0,rho=2700.)

model = Model(mesh,mat,sect)

appuis = Connections()
appuis.add_imposed_displ([n0],ux=[0],uy=[0],thetaz=[0])

forces = ExtForce()

K = model.assembl_stiffness_matrix()
M = model.assembl_mass_matrix()
L, Ud = model.assembl_connections(appuis)
F = model.assembl_external_forces(forces)
C = compute_Rayleigh_damping(K,M,alpha_K=0.,alpha_M=0.)

Nmodes = 2
omega2, xi = model.eigenmodes(K, M, L, Nmodes)

# Initial conditions for U and V
U0 = xi[0]
V0 = 0 * U0

fig = Figure(1,"Initial deformed state")
fig.plot_def(mesh,U0,0.1)
fig.show()

# Define Time Integrator object (Newmark method)
T0 = 0
Tfin = 50e-3
Nsteps = 100
time = np.linspace(T0, Tfin, Nsteps + 1)
evol = 0 * time
Newmark = TimeIntegrator(time, evol, beta=0.25, gamma=0.5)
U, V = Newmark.solve_evolution(U0, V0, K, C, M, F, L, Ud)
Etot = 0.5 * np.diag(U.T @ K @ U + V.T @ M @ V)

# Plot end node vertical displacement over time and compare to modal analysis
plt.figure(2)
d = nfin.get_dof("uy")  # position of end node deflection dof
U_first = U0[d]*np.cos(sqrt(omega2[0])*time)
plt.plot(time,U_first,label="First mode")

# =============================================================================
##           QUESTION 1.3.4 (A DECOMMENTER)
#
# q = [xi[i] @ M @ U0 for i in range(Nmodes)]
# print(q)
# U_modal = 0
# for i in range(Nmodes):
#     print("Modal participation %i : %f" % (i + 1, q[i] ** 2 / (U0 @ M @ U0)))
#     U_modal += q[i] * xi[i][d] * np.cos(sqrt(omega2[i]) * time)
# plt.plot(time, U_modal, label=str(Nmodes) + " first modes")
# =============================================================================

plt.plot(time,U[d,:],label="Transient analysis")
plt.xlabel("Time")
plt.ylabel("End node deflection")
Umax = np.max(np.abs(U0[:]))
plt.ylim((-1.4*Umax,1.4*Umax))
plt.legend(loc="best",fontsize=14)
plt.show()

# Plot total energy over time
plt.figure(3)
plt.title("Total energy")
plt.plot(time,Etot,label="Numerical energy")
plt.plot(time,Etot[0]+0*time,'--k',label="Initial energy")
plt.xlabel("Time")
plt.ylim((0,max(1.1*Etot[0],max(Etot))))
plt.legend(loc="best",fontsize=14)
plt.show()


# In[ ]:




