#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from wombat import *
import os

order = 2
if order == 1:
    elem_type = SolidT3
else:
    elem_type = SolidT6

mesh, boundary = call_gmsh(os.path.join('geometry', 'paroi.geo'), 
                           elem_type)

# physical boundaries
bottom = boundary.get_elem_from_tag("bottom")
sides = boundary.get_elem_from_tag("sides")
top = boundary.get_elem_from_tag("top")
wall_face = boundary.get_elem_from_tag("wall_face")
# physical regions
wall = mesh.get_elem_from_tag("wall")
soil = mesh.get_elem_from_tag("soil")


forces = ExtForce()

#==============================================================================
#
## A COMPLETER
#
mat_soil =
mat_wall = 

Uimp = 0.05
appuis = Connections()

model = Model(mesh, mat_soil)
model.affect_property( )
#==============================================================================


K = model.assembl_stiffness_matrix()
L, Ud = model.assembl_connections(appuis)
F = model.assembl_external_forces(forces)

U, lamb = model.solve(K, F, L, Ud)
Ux = U.get("ux")
Uy = U.get("uy")

#==============================================================================
Epot = 0
print("Potential energy:",Epot)
#==============================================================================

# extract "wall_face" nodes
extrados = NodeGroup(wall_face.get_nodes())
# get horizontal displacement from corresponding dofs
U_surf_x = U.get("ux", extrados)

plt.figure(1)
# sort along increasing y
y = extrados.coor[:,1]
idx = np.argsort(y)
plt.plot(U_surf_x[idx]*1e3,y[idx],'-ok')
plt.xlabel("Wall deflection [mm]")
plt.ylabel("Height $y$ [m]")
print("Maximum wall deflection:", max(U_surf_x))


# In[ ]:


fig = Figure(2,"Deformed shape")
fig.plot_def(mesh, U, 20, undef=False)
fig.plot_bc(mesh, appuis)
fig.show()


# In[ ]:


Sigma = model.stresses(U)
Sigxx = Sigma.get("sig_xx")
Sigyy = Sigma.get("sig_yy")
Sigzz = Sigma.get("sig_zz")
Sigxy = Sigma.get("sig_xy")

fig = Figure(3,r"$\sigma_{yy}$")
fig.plot_field(mesh, Sigyy)
fig.show()


# In[ ]:





# In[ ]:




