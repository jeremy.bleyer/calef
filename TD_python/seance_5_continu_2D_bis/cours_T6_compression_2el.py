#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from wombat import *

# ========================
#   A COMPLETER
n0 = Node2D([ ])
n1 =

el1 = SolidT6([ ])
el2 = SolidT6([ ])
# ========================

mesh = Mesh([el1, el2])

fig = Figure(1)
fig.plot(mesh)
fig.show()

mat = LinearElastic(E=100, nu=0.3)

# ========================
#   A COMPLETER
appuis = Connections()

# ========================

forces = ExtForce()

model = Model(mesh,mat)


K = model.assembl_stiffness_matrix()
L, Ud = model.assembl_connections(appuis)
F = model.assembl_external_forces(forces)

U, lamb = model.solve(K, F, L, Ud)
Ux = U.get("ux")
Uy = U.get("uy")

fig = Figure(1)
fig.plot_def(mesh,U,1.,undef=True)
fig.show()


# In[ ]:




