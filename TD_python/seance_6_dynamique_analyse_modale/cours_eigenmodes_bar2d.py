#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *
from math import sqrt, pi

l = 3.0
h = 1.0

n0 = Node2D([0, 0])
n1 = Node2D([l, 0])
n2 = Node2D([l, h])
n3 = Node2D([0, h])

el1 = Bar2D([n0, n1])
el2 = Bar2D([n1, n2])
el3 = Bar2D([n2, n3])

mesh = Mesh([el1, el2, el3])
sect = BeamSection().rect(1, 1)
mat = LinearElastic(1, 0, rho=1)

model = Model(mesh, mat, sect)

appuis = Connections()
appuis.add_imposed_displ([n0, n1, n2, n3], ux=[0, None, None, 0], uy=0)

K = model.assembl_stiffness_matrix()
M = model.assembl_mass_matrix()
M_lumped = model.assembl_mass_matrix(lumped=True)
L, Ud = model.assembl_connections(appuis)

print(np.array_str(M.toarray(), precision=2))
print(np.array_str(M_lumped.toarray(), precision=2))

Nmodes = 2
omega2, xi = model.eigenmodes(K, M, L, Nmodes)
omega2_l, xi_l = model.eigenmodes(K, M_lumped, L, Nmodes)

fig = Figure(2, "Eigenmodes", [Nmodes, 1])
for i in range(Nmodes):
    f_consistent = sqrt(omega2[i]) / 2 / pi
    f_lumped = sqrt(omega2_l[i]) / 2 / pi
    print(
        "Frequency: %f (consistent mass) | %f (lumped mass)" % (f_consistent, f_lumped)
    )
    fig.add_subplot(i)
    fig.plot_def(mesh, xi[i], 0.1)
plt.show()


# In[ ]:




