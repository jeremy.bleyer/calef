#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import *
from math import pi,sqrt,cos,cosh
from scipy.optimize import root

# Calcul de la solution exacte
falpha = lambda x: cos(x)*cosh(x)+1
alpha = lambda n: root(falpha,(2*n+1)*pi/2.)['x'][0]

length = 1.
E, nu = 70e9, 0.
rho = 2700.
a = 0.01

# Nombre d'elements dans la poutre
Nel = 1

n0 = Node2D([0,0])
n1 = n0
el_list = []
for k in range(Nel):
    n2 = Node2D([n1.coor[0]+length/Nel,0])
    el_list.append(Beam2D([n1,n2]))
    n1 = n2
mesh = Mesh(el_list)

sect = BeamSection().rect(a, a)
mat = LinearElastic(E, nu, rho=rho)

model = Model(mesh,mat,sect)

appuis = Connections()
appuis.add_imposed_displ([n0],ux=[0],uy=[0],thetaz=[0])

K = model.assembl_stiffness_matrix()
M = model.assembl_mass_matrix()
L, Ud = model.assembl_connections(appuis)

Nmodes = 1
omega2, xi = model.eigenmodes(K, M, L, Nmodes)

fig = Figure(2, "Eigenmodes", [(Nmodes + 1) // 2, 2])
for i in range(Nmodes):
    U = np.sqrt(xi[i].get("Ux") ** 2 + xi[i].get("Uy") ** 2)
    plt.subplot((Nmodes + 1) // 2, 2, i + 1)
    fig.plot_def(mesh, xi[i], 0.2 / max(abs(U)))
    plt.title("Eigenmode " + str(i + 1))
    plt.ylim(-0.25, 0.25)

    freq_FE = 0
    freq_ex = 0
    print("f_FE={:8.3f} Hz |  f_ex={:8.3f} Hz".format(freq_FE, freq_ex))

plt.tight_layout(pad=0)
fig.show()


# In[ ]:




