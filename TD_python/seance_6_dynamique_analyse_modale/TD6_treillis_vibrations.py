#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#!/usr/bin/env python
# -*- coding: utf-8 -*-
from wombat import *
from math import pi
import matplotlib.pyplot as plt

height = 1.0
half_length = 10.0
truss_type = "pratt"
model_type = "full"

ncell = int(half_length / height)
n0 = Node2D([-height, 0])
n1 = Node2D([0, 0])
n2 = Node2D([0, height])
el_list = [Bar2D([n0, n1]), Bar2D([n1, n2]), Bar2D([n2, n0])]
for k in range(ncell):
    n3 = Node2D([n1.coor[0] + height, 0])
    n4 = Node2D([n1.coor[0] + height, height])
    el_list.append(Bar2D([n1, n3]))
    if k == ncell - 1:
        el_list.append(Bar2D([n3, n4], tag=2))
    else:
        el_list.append(Bar2D([n3, n4]))
    el_list.append(Bar2D([n4, n2]))
    if truss_type == "pratt":
        el_list.append(Bar2D([n3, n2]))
    elif truss_type == "howe":
        el_list.append(Bar2D([n1, n4]))
    n1 = n3
    n2 = n4
if model_type == "full":
    for k in range(ncell):
        n3 = Node2D([n1.coor[0] + height, 0])
        n4 = Node2D([n1.coor[0] + height, height])
        el_list.append(Bar2D([n1, n3]))
        el_list.append(Bar2D([n3, n4]))
        el_list.append(Bar2D([n4, n2]))
        if truss_type == "pratt":
            el_list.append(Bar2D([n1, n4]))
        elif truss_type == "howe":
            el_list.append(Bar2D([n2, n3]))
        n1 = n3
        n2 = n4
    nfin = Node2D([n1.coor[0] + height, 0])
    el_list.append(Bar2D([n1, nfin]))
    el_list.append(Bar2D([n2, nfin]))

mesh = Mesh(el_list, Bar2D)
sect = BeamSection().rect(0.1, 0.1)
mat = LinearElastic(70e9, 0, rho=2700.0)
sect2 = BeamSection().rect(0.05, 0.1)

model = Model(mesh, mat, sect)

appuis = Connections()
if model_type == "full":
    appuis.add_imposed_displ([n0, nfin], ux=[0, 0], uy=[0, 0])
else:
    appuis.add_imposed_displ([n0, n1, n2], ux=[0, 0, 0], uy=[0, None, None])

fig = Figure(1)
fig.plot(mesh)
fig.plot_bc(mesh, appuis)
fig.show()

K = model.assembl_stiffness_matrix()
M = model.assembl_mass_matrix()
L, Ud = model.assembl_connections(appuis)

Nmodes = 4
omega2, xi = model.eigenmodes(K, M, L, Nmodes)

fig = Figure(2, "Eigenmodes", [(Nmodes + 1) // 2, 2])
for i in range(Nmodes):
    plt.subplot((Nmodes + 1) // 2, 2, i + 1)
    fig.plot_def(mesh, xi[i], 40)
    plt.title("Eigenmode " + str(i + 1))
    plt.ylim(-3, 3)
    plt.gca().set_aspect("equal")
plt.tight_layout(pad=0)
fig.show()


# In[ ]:




