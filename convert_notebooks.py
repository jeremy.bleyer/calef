# -*- coding: utf-8 -*-
import sys
import os
import shutil

TD_notebooks = "TD_notebooks"
TD_python = "TD_python"


def process():

    # Directory to scan
    demos = os.path.join(os.path.dirname(os.path.abspath(__file__)), TD_notebooks)

    # Destination directory
    py_demos = os.path.join(os.path.dirname(os.path.abspath(__file__)), TD_python)

    # Remove python scripts
    if os.path.exists(py_demos):
        shutil.rmtree(py_demos)
    # Copy notebook scripts
    shutil.copytree(demos, py_demos)

    subdir = [f.path for f in os.scandir(demos) if f.is_dir()]

    ipynb_files = [
        os.path.join(demo, os.path.split(f)[1])
        for demo in subdir
        for f in os.listdir(demo)
        if f.endswith("ipynb") and "checkpoint" not in f
    ]
    # loop over notebook files
    for f in ipynb_files:
        demo_dir = os.path.basename(os.path.dirname(f))
        file_name = os.path.split(f)[1].replace(".ipynb", "")
        fpy = f.replace(".ipynb", ".py").replace(TD_notebooks, TD_python)

        # clear cell outputs
        os.system(
            "jupyter-nbconvert --ClearOutputPreprocessor.enabled=True --clear-output --inplace {}".format(
                f
            )
        )

        # convert to script in python dir
        ret = os.system(
            "jupyter-nbconvert --to script {} --output {} --output-dir {}".format(
                f, file_name, os.path.join(".", TD_python, demo_dir)
            )
        )

        # remove notebook from python folder
        os.remove(fpy.replace(".py", ".ipynb"))
        # remove ipython magic commands from python script
        remove_ipython_magic(fpy)


def remove_ipython_magic(file):
    with open(file, "r+") as f:
        new_f = f.readlines()
        f.seek(0)
        for line in new_f:
            if "get_ipython().run_line_magic" not in line:
                f.write(line)
        f.truncate()


if __name__ == "__main__":
    process()