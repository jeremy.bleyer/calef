# Cours Eléments finis pour le génie civil (Ecole des Ponts ParisTech)

**Responsable** : Jérémy Bleyer, jeremy.bleyer@enpc.fr

# Utilisation en ligne avec **Binder**

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.enpc.fr%2Fjeremy.bleyer%2Fcalef/master?urlpath=lab/tree/TD_notebooks)

# Installation en local

0. Vous devez disposer d'une distribution **Python 3** (via Anaconda par exemple) contenant les paquet suivant *numpy*, *matplotlib*, *scipy*, *meshio*
1. [Télécharger les fichiers](https://gitlab.enpc.fr/jeremy.bleyer/calef/repository/archive.zip?ref=master) du répertoire et dézipper l'ensemble dans le dossier de votre choix.
2. [Télécharger le mailleur Gmsh](https://gmsh.info/#Download), dézipper le fichier.
3. Deux possibilités:
  * Ajouter Gmsh à vos variables d'environnement pour pouvoir l'exécuter depuis n'importe quel dossier.
  * ou bien, placer l'exécutable (`gmsh.exe` par exemple sous Windows) dans le dossier `wombat`
4. Installer le module Wombat:
  * Démarrer le PowerShell Anaconda
  * Changer le répertoire en allant jusqu'à la racine du projet en faisant `cd mon/chemin/vers/le/projet` 
  * Une fois dans le répertoire du projet, exécuter `python setup.py develop --user`
5. Tester l'installation en exécutant `test_installation.py`

Vous pouvez ensuite travailler avec les TD sous forme de scripts Python (dossier `TD_python`) ou sous forme de notebooks Jupyter (dossier `TD_notebooks`).