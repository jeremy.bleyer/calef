# -*- coding: utf-8 -*-
"""
Module for nonlinear computations

This file is part of the **WomBat** finite element code
used for the *Civil Engineering Finite Element Course*
of *Ecole des Ponts ParisTech* 2017-2018

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC,IFSTTAR,CNRS UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
import numpy as np
from .utils import value_at_next_step
from .fields import ElementField
import scipy.sparse as sparse


def nonlinear_solver(
    model, evol, F_list, L, Ud_list, Sig, X, method, tol, nitermax, print_info
):
    solver_parameters = {
        "method": method,
        "print_info": print_info,
        "tol": tol,
        "max_iterations": nitermax,
    }
    mesh = model.mesh
    Nd = mesh.Ndof

    nincr = len(evol)
    U_list = np.zeros((Nd, nincr))
    lamb = np.zeros((L.shape[0],))
    if isinstance(Sig, ElementField):
        Sig = Sig._array
    elif not (isinstance(Sig, np.ndarray)):
        Sig = np.zeros((mesh.Nsig,))
    if not (isinstance(X, np.ndarray)):
        X = np.zeros((sum(mesh.get_nb_internal_var()),))
        ivar_offsets = np.cumsum(np.insert(mesh.get_nb_internal_var(), 0, 0))
        for (i, e) in enumerate(mesh.elem_list):
            X[ivar_offsets[i] : ivar_offsets[i + 1]] = getattr(e, "internal_var", 0)

    K_newton = model.assembl_stiffness_matrix()
    Fint = np.zeros((Nd,))
    Fint, new_Sig, dX, _ = internal_forces(
        model, np.zeros((Nd,)), Sig, X, tangent_matrix=True
    )

    last_Ud = 0 * lamb
    Sig_list = [to_element_field(Sig, mesh)]
    X_list = np.repeat(X[:, np.newaxis], nincr, axis=1)
    lamb_list = np.repeat(lamb[:, np.newaxis], nincr, axis=1)
    info_list = []
    for (i, t) in enumerate(evol[1:]):
        new_F = value_at_next_step(F_list, t)
        new_Ud = value_at_next_step(Ud_list, t)
        print(new_Ud)
        DU, lamb, Sig, X, Fint, K_newton, info = solve_increment(
            model,
            K_newton,
            new_F,
            L,
            new_Ud - last_Ud,
            lamb,
            Sig,
            X,
            Fint,
            solver_parameters,
        )
        U_list[:, i + 1] = U_list[:, i] + DU
        Sig_list.append(to_element_field(Sig, mesh))
        X_list[:, i + 1] = X
        lamb_list[:, i + 1] = lamb
        info_list.append(info)
        last_Ud = new_Ud
        if print_info:
            print("Increment %i : Converged in %i iterations" % (i + 1, info["niter"]))

    return U_list, lamb_list, Sig_list, X_list, info_list


def to_element_field(Sig_arr, mesh):

    stress_offsets = np.cumsum(np.insert(mesh.get_nb_stresses(), 0, 0))
    Sig = ElementField(mesh.elem_list, mesh.Nsig)
    for (i, e) in enumerate(mesh.elem_list):
        sig_el = Sig_arr[stress_offsets[i] : stress_offsets[i + 1]]
        Sig.update(e, {key: val for (key, val) in zip(e.stresses_field_names, sig_el)})
    return Sig


def solve_increment(
    model,
    K,
    Fext,
    L,
    Ud,
    lamb,
    Sig,
    X,
    Fint,
    solver_parameters={
        "max_iterations": 100,
        "method": "newton-raphson",
        "print_info": False,
        "tol": 1e-6,
    },
):
    """
    Solves a nonlinear increment using the Newton method

    Parameters
    ----------
    K : sparse matrix
        stiffness matrix
    Fext : ndarray
        external force vector at current time step
    L : sparse matrix
        connection matrix
    Ud : ndarray
        vector of imposed displacement at given time step
    lamb : ndarray
        Lagrange multiplier at previous time step
    Sig : ndarray
        stress state at previous time step
    X : ndarray
        internal variables at previous time step
    Fint : ndarray
        vector of internal forces at previous time step
    model
        :class:`Model <model.Model>` object
    solver_parameters
        dictionnary containing the solver parameters
    """
    sp = solver_parameters
    nitermax = sp["max_iterations"]
    if sp["method"] == "newton-raphson":
        tangent_matrix = True
    else:
        tangent_matrix = False

    DU = np.zeros((K.shape[0],))
    DX = 0 * X
    Res = Fint + L.T.dot(lamb) - Fext
    nRes0 = max(np.linalg.norm(Res), np.linalg.norm(Ud))
    nRes = nRes0
    new_Sig = Sig
    flag_Ud = 1
    niter = 0
    info = {"niter": 0, "residual": nRes0}
    K_newton = K
    while (nRes / nRes0 > sp["tol"]) and (niter < nitermax):
        niter += 1
        dU, dlamb = model.solve(K_newton, -Res, L, flag_Ud * Ud)
        DU += dU
        lamb += dlamb
        Fint, new_Sig, dX, Kt = internal_forces(
            model, DU, Sig, X, tangent_matrix=tangent_matrix
        )
        if tangent_matrix:
            K_newton = Kt
        else:
            K_newton = K
        DX = dX
        Res = Fint + L.T.dot(lamb) - Fext
        nRes = np.linalg.norm(Res)
        info["niter"] += 1
        info["residual"] = nRes / nRes0
        if sp["print_info"]:
            print(
                "     Iteration %i Normalized residual norm %e" % (niter, nRes / nRes0)
            )
        flag_Ud = 0
    if niter == nitermax:
        raise ValueError(
            "Newton-Raphson method did not converge in less than %i iterations \
            \n Residual norm is %e"
            % (nitermax, nRes / nRes0)
        )
    return DU, lamb, new_Sig, X + DX, Fint, K_newton, info


def internal_forces(model, DU, Sig, X, tangent_matrix=False):
    mesh = model.mesh
    new_Sig = np.zeros((mesh.Nsig,))

    dX = np.zeros((sum(mesh.get_nb_internal_var()),))
    Nd = mesh.Ndof
    Fint = np.zeros((Nd,))
    stress_offsets = np.cumsum(np.insert(mesh.get_nb_stresses(), 0, 0))
    ivar_offsets = np.cumsum(np.insert(mesh.get_nb_internal_var(), 0, 0))

    if tangent_matrix:
        Nb_non_zero = mesh.Nel * mesh.el_dof ** 2
        Kt = sparse.coo_matrix((Nd, Nd))
        Kt.data = np.zeros((Nb_non_zero,), "float64")
        Kt.row = np.zeros((Nb_non_zero,), dtype="int32")
        Kt.col = np.zeros((Nb_non_zero,), dtype="int32")
        buff = 0

    for (i, e) in enumerate(mesh.elem_list):
        dofe = e.get_dofs()
        fint_el, sig_el, dX_el, Kte = e.internal_forces_nl(
            DU[e.get_dofs()],
            Sig[stress_offsets[i] : stress_offsets[i + 1]],
            X[ivar_offsets[i] : ivar_offsets[i + 1]],
            e.mat,
            e.sect,
        )

        if tangent_matrix:
            Kte = sparse.coo_matrix(Kte)
            nnz = Kte.data.shape[0]
            Kt.data[buff : buff + nnz] = Kte.data
            Kt.row[buff : buff + nnz] = dofe[Kte.row]
            Kt.col[buff : buff + nnz] = dofe[Kte.col]
            buff += nnz
        new_Sig[stress_offsets[i] : stress_offsets[i + 1]] = sig_el
        dX[ivar_offsets[i] : ivar_offsets[i + 1]] = dX_el
        Fint[dofe] += fint_el
    if tangent_matrix:
        return Fint, new_Sig, dX, Kt
    else:
        return Fint, new_Sig, dX, 0
