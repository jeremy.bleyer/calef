"""Module implementing field classes."""
import numpy as np
from .node import NodeGroup


class NodalField:
    """A nodal field object."""

    def __init__(self, nodes, array):
        self.nodes = nodes
        self._array = array
        self.subfield_names = set(
            [k for n in self.nodes.node_list for k in n._dofs.keys()]
        )

    def array(self):
        """Get array of nodal values."""
        return self._array

    def get(self, name, location=None):
        """Get subfield from given name."""
        if location is None:
            node_list = self.nodes.node_list
        elif isinstance(location, NodeGroup):
            node_list = location.node_list
        else:
            node_list = location
        node_view = [n for n in node_list if name.lower() in n._dofs.keys()]
        dof_view = np.array([n.get_dof(name.lower()) for n in node_view])
        return self._array[dof_view]

    def __getitem__(self, item):
        """Get item."""
        return self._array[item]

    def __str__(self):
        return str(self._array)

    def __rmul__(self, other):
        if isinstance(other, float) or isinstance(other, int):
            return NodalField(self.nodes, other * self._array)
        elif isinstance(other, np.ndarray):
            return other @ self._array
        elif isinstance(other, NodalField):
            return other._array @ self._array

    def __mul__(self, other):
        return self.__rmul__(other)

    def __matmul__(self, other):
        if isinstance(other, NodalField):
            return other._array @ self._array
        else:
            return self._array @ other

    def __rmatmul__(self, other):
        return self.__matmul__(other.T)

    def __array__(self):
        return self._array

    def T(self):
        return self._array


class ElementField:
    """An element field object."""

    def __init__(self, elements, Nval):
        self.elements = elements
        self.data = {e: None for e in elements}
        self.subfield_names = set()
        self._array = np.zeros((Nval,))

    def update(self, e, data):
        """Update field with data from element e."""
        self.data[e] = data
        for key in data.keys():
            self.subfield_names.add(key)

    def _data_from_key(self, key):
        def get_key(data, keys):
            try:
                val = data[key]
            except KeyError:
                val = [data[keys] for keys in data.keys() if keys.startswith(key)]
                if len(val) == 0:
                    val = None
            return val

        return {e: get_key(self.data[e], key) for e in self.elements}

    def get(self, name):
        """Get subfield from given name."""
        data = self._data_from_key(name)
        size = max(
            [len(val) if hasattr(val, "__len__") else 0 for val in data.values()]
        )
        if size == 0:
            array = np.zeros((len(self.elements),))
        else:
            array = np.zeros((len(self.elements), size))

        for i, e in enumerate(self.elements):
            val = data[e]
            if val is not None:
                if size > 0:
                    array[i, :] = np.asarray(val).flatten()
                else:
                    array[i] = val
        return array

    def __str__(self):
        """String representation."""
        s = ""
        names = list(self.subfield_names)
        names.sort()
        for key in names:
            data = self.get(key)
            s += f"{key}:\n{data}\n"
        return s
