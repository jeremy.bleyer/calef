# -*- coding: utf-8 -*-
"""
Module for class `Model`

This file is part of the **WomBat** finite element code
used for the *Civil Engineering Finite Element Course*
of *Ecole des Ponts ParisTech* 2017-2018

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC,IFSTTAR,CNRS UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from .nonlinear import nonlinear_solver
from .material import Material
from .geometric_caract import Section
from .element.generic_element import ElementGroup
from .fields import NodalField, ElementField
from .linear_algebra import solve, eigenmodes

import numpy as np
import scipy.sparse as sparse


class Model:
    """Contains a mesh, material and (possibly) section properties

    Attributes
    ----------
    mesh
        :class:`Mesh <mesh.Mesh>` object
    mat
        :class:`Material <material.Material>` object
    sect
        :class:`Section <geometric_caract.Section>` object (optional)
    """

    def __init__(self, mesh, mat, sect=Section()):
        self.mesh = mesh
        self.mat = mat
        self.sect = sect
        self.affect_property(mat)
        self.affect_property(sect)

    def affect_property(self, prop, el_list=[], tag=None):
        """Affect property (material or section) to a specific list of elements

        Parameters
        ----------
        prop : {:class:`Material <material.Material>`, :class:`Section <geometric_caract.Section>`}
            property to affect, either a material or a section
        el_list : list, :class:`ElementGroup`
            list of elements on which `prop` is affected

            if list is empty, all elements of the mesh are selected
        tag : None, int, string
            if tag is not None, only the elements of `el_list`
            with `tag` as a physical region marker are selected
        """
        if isinstance(el_list, ElementGroup):
            el_list = el_list.elem_list

        assert (len(el_list) == 0) or (
            tag is None
        ), "Element list and tag cannot be affected at the same time"

        if len(el_list) == 0 and tag is None:
            to_do_list = self.mesh.elem_list
        elif len(el_list) != 0:
            to_do_list = el_list
        elif tag is not None:
            to_do_list = self.mesh.get_elem_from_tag(tag).elem_list

        for e in to_do_list:
            if isinstance(prop, Material):
                e.mat = prop
            elif isinstance(prop, Section):
                e.sect = prop
            else:
                raise ValueError("Unknown property type")

    def assembl_connections(self, connections):
        """Assembly procedure for relations between dofs

        Parameters
        ----------
        connections
            :class:`Connections <connections.Connections>` object

        model
            :class:`Model <model.Model>` object

        Returns
        -------
        L : sparse lil matrix
            :math:`[L]` connection matrix of shape (Nl,Nd) where
            Nl number of relations and Nd number of dofs
        Ud : ndarray
            :math:`\{U_d\}` imposed displacement vector of shape (Nl,)
        """
        mesh = self.mesh
        Nd = mesh.Ndof
        Nl = connections.nb_relations
        L = np.zeros((Nl, Nd))
        Ud = np.zeros((Nl,))
        buff = 0
        for i, master_dof in enumerate(connections.master_list):
            slaves = connections.slave_list[i]
            for j, slave_dof in enumerate(slaves):
                if master_dof is not None:
                    L[buff, master_dof] = connections.lin_rela_list[i][0]
                if len(connections.lin_rela_list[i]) == 2:
                    jrela = 1
                else:
                    jrela = j + 1
                L[buff, slave_dof] = connections.lin_rela_list[i][jrela]
            Ud[buff] = connections.imposed_value_list[i]
            buff += 1

        sorted_idx = np.lexsort(L.T)
        sorted_data = L[sorted_idx, :]

        # Get unique row mask
        row_mask = np.append([True], np.any(np.diff(sorted_data, axis=0), 1))

        # Get unique rows
        return sparse.coo_matrix(sorted_data[row_mask, :]), Ud[sorted_idx[row_mask]]

    def assembl_stiffness_matrix(self):
        """Assembly procedure of the global stiffness matrix.

        Parameters
        ----------
        model
            :class:`Model <model.Model>` object

        Returns
        -------
        K : sparse coo matrix
            :math:`[K]` global stiffness matrix of shape=(Nd,Nd) where Nd number of dofs
        """
        mesh = self.mesh
        Nd = mesh.Ndof
        Nb_non_zero = mesh.Nel * mesh.el_dof**2
        K = sparse.coo_matrix((Nd, Nd))
        K.data = np.zeros((Nb_non_zero,), dtype="float64")
        K.row = np.zeros((Nb_non_zero,), dtype="int32")
        K.col = np.zeros((Nb_non_zero,), dtype="int32")
        buff = 0

        for e in mesh.elem_list:
            dofe = e.get_dofs()
            # call elementary stiffness method
            Ke = sparse.coo_matrix(e.elementary_stiffness(e.mat, e.sect))

            nnz = Ke.data.shape[0]
            K.data[buff : buff + nnz] = Ke.data
            K.row[buff : buff + nnz] = dofe[Ke.row]
            K.col[buff : buff + nnz] = dofe[Ke.col]
            buff += nnz

        return K

    def assembl_external_forces(self, forces):
        """Assembly procedure of the external forces vector

        Parameters
        ----------
        forces
            :class:`ExtForce <forces.ExtForce>` object
        model
            :class:`Model <model.Model>` object

        Returns
        -------
        F : ndarray
            :math:`\{F\}` vector of equivalent external forces shape=(Nd,)
            where Nd number of dofs
        """
        mesh = self.mesh
        Ndof = mesh.Ndof
        F = np.zeros((Ndof,))
        for index, node in enumerate(forces.node_list):

            for comp, values in forces.concentrated.items():
                dof_name = node._force_to_dof_name[comp.lower()]
                if values[index] is not None:
                    F[node.get_dof(dof_name)] += values[index]
        for i, e in enumerate(forces.el_list):
            el_forces = {
                comp.lower(): forces.distributed[comp.lower()][i]
                for comp in forces.distributed.keys()
            }
            force_values = []
            for key, values in el_forces.items():
                if key not in [n.lower() for n in e.force_names]:
                    raise KeyError(key)
            for comp in e.force_names:
                force_values.append(el_forces.get(comp.lower(), 0))

            dofe = e.get_dofs()

            F[dofe] += e.elementary_distributed_forces(force_values)

        return F

    def stresses(self, U):
        """Compute generalized stresses (elastic behaviour)

        Parameters
        ----------
        U : ndarray
            displacement vector solution :math:`\{U\}`

        Returns
        -------
        Sig : ndarray
            vector of generalized stresses :math:`\{\Sigma\}` (depends on the element)
        """
        mesh = self.mesh
        Sig = ElementField(mesh.elem_list, mesh.Nsig)
        stress_offsets = np.cumsum(np.insert(mesh.get_nb_stresses(), 0, 0))
        for i, e in enumerate(mesh.elem_list):
            sig_el = e.stresses(U[e.get_dofs()], e.mat, e.sect)
            if hasattr(e, "sig0"):
                sig_el += np.asarray(e.sig0)
            if isinstance(sig_el, float):
                sig_el = [sig_el]
            Sig.update(
                # e, {key: val for (key, val) in zip(e.stresses_field_names, sig_el)}
                e,
                {
                    key: sig_el[i :: len(e.stresses_field_names)]
                    for i, key in enumerate(e.stresses_field_names)
                },
            )
            Sig._array[stress_offsets[i] : stress_offsets[i + 1]] = sig_el
        return Sig

    def assembl_mass_matrix(self, lumped=False):
        """Assembly procedure of the global mass matrix

        Parameters
        ----------
        model
            :class:`Model <model.Model>` object
        lumped : bool
            if True compute the lumped mass matrix (when available), otherwise compute the consistent mass matrix (default)

        Returns
        -------
        M : sparse coo matrix
            :math:`[M]` global mass matrix of shape=(Nd,Nd) where Nd number of dofs
        """
        mesh = self.mesh
        Nd = mesh.Ndof
        Nb_non_zero = mesh.Nel * mesh.el_dof**2
        M = sparse.coo_matrix((Nd, Nd))
        M.data = np.zeros((Nb_non_zero,), dtype="float64")
        M.row = np.zeros((Nb_non_zero,), dtype="int32")
        M.col = np.zeros((Nb_non_zero,), dtype="int32")
        buff = 0

        for e in mesh.elem_list:
            dofe = e.get_dofs()

            # call elementary mass method
            Me = sparse.coo_matrix(e.elementary_mass(e.mat, e.sect, lumped))

            nnz = Me.data.shape[0]
            M.data[buff : buff + nnz] = Me.data
            M.row[buff : buff + nnz] = dofe[Me.row]
            M.col[buff : buff + nnz] = dofe[Me.col]
            buff += nnz

        return M

    def assembl_geometric_stiffness_matrix(self, U):
        """Assembly procedure of the global geometric stiffness matrix

        Parameters
        ----------
        U : ndarray
            global displacement vector on which geometrical stiffness effects are evaluated
        model
            :class:`Model <model.Model>` object

        Returns
        -------
        KG : sparse coo matrix
            :math:`[K_G]` global geometric stiffness matrix of shape=(Nd,Nd) where Nd number of dofs
        """
        mesh = self.mesh
        Nd = mesh.Ndof
        Nb_non_zero = mesh.Nel * mesh.el_dof**2
        KG = sparse.coo_matrix((Nd, Nd))
        KG.data = np.zeros((Nb_non_zero,), dtype="float64")
        KG.row = np.zeros((Nb_non_zero,), dtype="int32")
        KG.col = np.zeros((Nb_non_zero,), dtype="int32")
        buff = 0

        for e in mesh.elem_list:
            dofe = e.get_dofs()

            # compute internal forces
            Fint_e = e.stresses(U[dofe], e.mat, e.sect)
            # call elementary geometric stiffness method
            KGe = sparse.coo_matrix(
                e.elementary_geometric_stiffness(Fint_e, e.mat, e.sect)
            )

            nnz = KGe.data.shape[0]
            KG.data[buff : buff + nnz] = KGe.data
            KG.row[buff : buff + nnz] = dofe[KGe.row]
            KG.col[buff : buff + nnz] = dofe[KGe.col]
            buff += nnz

        return KG

    def assembl_thermal_strains(self):
        """Assembly procedure of the thermal strain vector

        Parameters
        ----------
        dilat : ndarray
            array of shape (Nel,) containing thermal dilatation of each element
        model
            :class:`Model <model.Model>` object

        Returns
        -------
        Fther : ndarray
            :math:`\{F_{ther}\}` vector of equivalent thermal forces shape=(Nd,)
            where Nd number of dofs
        """
        mesh = self.mesh
        Nd = mesh.Ndof
        Fther = np.zeros((Nd,))
        for i, e in enumerate(mesh.elem_list):
            dofe = e.get_dofs()
            Fther[dofe] += e.elementary_thermal_vector(e.mat, e.sect)
        return Fther

    def assembl_initial_state(self):
        """Assembly procedure of the initial state internal force vector :math:`\{F^{int,0}\}`

        Parameters
        ----------
        Sig : ndarray
            array containing initial stress state
        model
            :class:`Model <model.Model>` object

        Returns
        -------
        Fint : ndarray
            :math:`\{F^{int,0}\}` vector of equivalent internal forces shape=(Nd,)
            where Nd number of dofs
        """
        mesh = self.mesh
        Nd = mesh.Ndof
        Fint = np.zeros((Nd,))

        for i, e in enumerate(mesh.elem_list):
            dofe = e.get_dofs()
            if hasattr(e, "sig0"):
                Fint[dofe] += e.internal_forces(e.sig0)

        return Fint

    def solve(self, K, F, L, Ud):
        """Resolution of the global finite element linear system using Lagrange multipliers

        :math:`\\begin{bmatrix} K & L^T \\\\ L & 0 \end{bmatrix}\\begin{Bmatrix} U \\\\ \lambda \end{Bmatrix}=
        \\begin{Bmatrix} F \\\\ U_d \end{Bmatrix}`

        Parameters
        ----------
        K : sparse matrix
            global stiffness matrix :math:`[K]` shape (Nd,Nd)
        F : ndarray
            global forces vector :math:`\{F\}` shape (Nd,)
        L : sparse matrix
            connection matrix :math:`[L]` shape (Nl,Nd)
        Ud : ndarray
            imposed displacement vector :math:`\{U_d\}` shape (Nl,)

        Returns
        -------
        U : ndarray
            :math:`\{U\}` displacement vector shape (Nd,)
        lamb : ndarray
            :math:`{\lambda}\}` Lagrange multiplier vector shape (Nl,)
        """
        U, lamb = solve(K, F, L, Ud)
        return NodalField(self.mesh.nodes, U), lamb

    def eigenmodes(
        self, K, B, L=[], nmodes=1, mode="normal", shift=None, compute_error=False
    ):
        """Extraction of eigenmodes for the generalized eigenvalue problem

        :math:`[K]\{\\xi\\} = \\pm\\lambda[B]\{\\xi\}`

            sign :math:`+` for mode="normal", :math:`-` for mode="buckling"

        Parameters
        ----------
        K : sparse matrix
            global stiffness matrix
        B : sparse matrix
            :math:`[B]=[M]` mass matrix (with mode="normal") or :math:`[B]=[K_G]`
            geometric stiffness matrix (with mode="buckling")
        L : sparse matrix
            connection matrix :math:`[L]` shape (Nl,Nd)
        nmodes : int
            number of modes with smallest eigenvalues to extract, must be less than matrix rank
        mode : {"normal","buckling"}
            + "normal" : eigenvalue problem for dynamic modal analysis :math:`[K]\{\\xi\\} = \\lambda[M]\{\\xi\}`
            + "buckling" : eigenvalue problem for buckling analysis :math:`[K]\{\\xi\\} = -\\lambda[K_G]\{\\xi\}`
        shift : float
            value around which eigenvalues are looked for (improves accuracy, especially for "buckling" mode)
        compute_error : bool
            if True, prints the residual norm :math:`\|[K]\{\\xi\}\mp \\lambda[B]\{\\xi\}\|_2`

        Returns
        -------
        lamb : ndarray shape = (nmodes,)
            array of eigenvalues :math:`\\lambda`
        xi : ndarray shape = (Nd,nmodes)
            array of eigenmodes :math:`\{\\xi\}`
        """
        sig, xi = eigenmodes(K, B, L, nmodes, mode, shift, compute_error)
        return sig, [NodalField(self.mesh.nodes, xi[:, i]) for i in range(nmodes)]

    def nonlinear_solver(
        self,
        evol,
        F_list,
        L,
        Ud_list,
        Sig=0,
        X=0,
        method="newton-raphson",
        tol=1e-6,
        nitermax=100,
        print_info=False,
    ):
        """Nonlinear solver using a Newton method

        Parameters
        ----------
        evol : list, ndarray
            value of the loading amplitude at the corresponding time
        F_list : ndarray or list
            external reference force vector, amplitude is multiplied by evol
            :math:`F(t) = F_{list}[0]\\cdot evol(t)+F_{list}[1]` if two elements in F_list
        L : sparse matrix
            connection matrix
        Ud_list : ndarray or list
            imposed displacements reference vector, amplitude is multiplied by evol
            :math:`Ud(t) = Ud_{list}[0]\\cdot evol(t)+Ud_{list}[1]` if two elements in Ud_list
        model
            :class:`Model <model.Model>` object
        Sig : ndarray
            initial stress state (default is natural state)
        X : ndarray
            initial internal variable vector (default is zero)
        method : {"newton-raphson","modified-newton"}
            method type (use of tangent matrix or not)
        tol : float
            stopping criterion tolerance (default = 1e-6)
        nitermax : int
            maximum number of iterations before stopping an increment (default = 100)
        print_info : bool
            print iteration informations if True

        Returns
        -------
        U_list : ndarray
            displacement vector at all Nsteps time steps shape=(Nd,Nsteps)
        lamb_list : ndarray
            vector of Lagrange multipliers at all time steps
        Sig_list : ndarray
            vector of stress state at all time steps
        lX_list : ndarray
            vector of internal variables at all time steps
        info_list : list
            list of algorithm information at all time steps
        """
        return nonlinear_solver(
            self,
            evol,
            F_list,
            L,
            Ud_list,
            Sig,
            X,
            method,
            tol,
            nitermax,
            print_info,
        )
