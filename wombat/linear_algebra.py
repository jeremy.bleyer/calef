"""Linear algebra utility functions."""

from scipy import linalg
import scipy.sparse as sparse
import scipy.sparse.linalg as slin
import numpy as np
from time import time


def solve(K, F, L=[], Ud=[], print_info=False):
    n = L.shape[0]
    Z = np.zeros((n, n))
    # Building complete system [A] = [[K, L^T],[L,0]]
    A = sparse.vstack((sparse.hstack((K, L.T)), sparse.hstack((L, Z)))).tocsc()
    # Building complete right-hand-side {b} = {F,Ud}
    b = np.hstack((F, Ud)).T
    # solving linear system [A]{x} = {b}
    tic = time()
    fact = slin.splu(A)
    x = fact.solve(b)

    toc = time()
    if print_info:
        print("Linear solver time : %f s" % (toc - tic))
    U, lamb = x[0 : K.shape[0]], x[K.shape[0] :]
    return U, lamb


def eigenmodes(K, B, L, nmodes, mode, shift, compute_error):
    tic = time()
    Z = sparse.csc_matrix(null(L.toarray()))
    r = Z.shape[1]
    Kred = Z.T.dot(K.dot(Z))
    Bred = Z.T.dot(B.dot(Z))
    if mode == "normal":
        sign = 1.0
        if shift == None:
            shift = 0
    elif mode == "buckling":
        sign = -1.0
        if shift == None:
            # Needs an estimate of the minimum eigenvalue
            shift = slin.norm(B) / slin.norm(K, ord=-np.inf)
    if nmodes < r:
        sig, v = slin.eigsh(
            Kred, nmodes, sign * Bred, sigma=shift, which="LM", mode=mode
        )
    elif nmodes == r:
        sig, v = linalg.eigh(Kred.toarray(), sign * Bred.toarray())
    else:
        raise ValueError(
            "Requested number of modes "
            + str(nmodes)
            + " is larger than number of dofs "
            + str(r)
        )
    reorder = np.argsort(np.abs(sig))
    sig = sig[reorder]
    v = v[:, reorder]

    if compute_error:
        for i in range(nmodes):
            print(
                "Error on mode %i: %e"
                % (
                    i,
                    np.linalg.norm(
                        Kred.dot(v[:, i]) - sign * sig[i] * Bred.dot(v[:, i])
                    )
                    / np.linalg.norm(sig[i] * Bred.dot(v[:, i])),
                )
            )
    toc = time()
    print("Eigensolver time : %f s\n" % (toc - tic))
    return sig, Z.dot(v)


def null(A, eps=1e-12):
    """Compute null space of matrix A up to a tolerance eps"""
    u, s, vh = linalg.svd(A)
    padding = max(0, np.shape(A)[1] - np.shape(s)[0])
    null_mask = np.concatenate(((s <= eps), np.ones((padding,), dtype=bool)), axis=0)
    null_space = np.compress(null_mask, vh, axis=0)
    return null_space.T


def compute_Rayleigh_damping(K, M, alpha_K=0, alpha_M=0):
    """Returns Rayleigh damping matrix :math:`[C] = \\alpha_K[K]+\\alpha_M[M]`"""
    return sparse.coo_matrix(alpha_K * K + alpha_M * M)
