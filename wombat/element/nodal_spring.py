# -*- coding: utf-8 -*-
"""
Module for class `Bar2D`

This file is part of the **WomBat** finite element code
used for the *Civil Engineering Finite Element Course*
of *Ecole des Ponts ParisTech* 2017-2018

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC,IFSTTAR,CNRS UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from .generic_element import Segment
import numpy as np


class NodalSpring(Segment):
    """ A spring element connecting two nodes

        - **Kinematics**: horizontal, vertical displacement \
        and rotation

            .. math:: \{U\}=\\langle u_x^1,u_y^1,\theta_z^1,u_x^2,u_y^2,theta_z^2\\rangle^T

        - **Strains**: displacement jump :math:`\delta=v^2-v^1`  (constant)
        - **Stresses**: generalized force :math:`R` (constant)
    """

    def __init__(self, node_list, tag=1):
        """
        Parameters
        ----------

        node_list : list
            list containing two nodes   
        tag : int,str
            tag of physical group
        """
        Segment.__init__(self, node_list, tag)
        self.el_dof = 6
        self.node_dof = 3
        self.nb_stresses = 3
        self.dof_names = ["Ux", "Uy", "Thetaz"]
        self.force_names = ["Fx", "Fy", "Cz"]
        self.strain_field_names = ["delta_x", "delta_y", "omega_z"]
        self.stresses_field_names = ["Rx", "Ry", "Mz"]
        self.dilat = 0

    def elementary_stiffness(self, mat, sect):
        """Elementary stiffness matrix :math:`[K_e]` shape=(4,4)

        elementary stiffness in local frame is

        .. math:: [K_{e,loc}]=\\dfrac{ES}{L}\\begin{bmatrix} 1 & -1 \\\\ -1 & 1\end{bmatrix}
        """
        k = mat.spring_stiffness

        # elementary stiffness matrix
        return np.kron(np.array([[1, -1], [-1, 1]]), np.diag(k))

    def elementary_distributed_forces(self, el_force):
        raise ValueError("Distributed forces cannot be applied to a spring element.")

    # ==============================================================================

    def elementary_thermal_vector(self, mat, sect):
        """ Elementary force vector induced by a thermal strain """
        raise NotImplementedError

    # ==============================================================================

    def deformation(self, Ue):
        """Interpolation of the deformed element

        Parameters
        ----------
        Ue : ndarray
            nodal displacement of the current element

        Returns
        -------
        x_def,y_def : ndarray
            returns deformed position of the two end nodes
        """
        Ux = Ue[0 :: self.node_dof]
        Uy = Ue[1 :: self.node_dof]
        s = np.linspace(-1, 1, 2)
        x = self.node_coor()[:, 0]
        y = self.node_coor()[:, 1]
        x_def = (1 - s) / 2.0 * (Ux[0] + x[0]) + (1 + s) / 2.0 * (Ux[1] + x[1])
        y_def = (1 - s) / 2.0 * (Uy[0] + y[0]) + (1 + s) / 2.0 * (Uy[1] + y[1])
        return [], []

    def stresses(self, Ue, mat, sect):
        """Compute generalized stresses

            .. math:: \{\Sigma\} = \{N\}

        Parameters
        ----------
        Ue : ndarray
            nodal values of the displacement
        """
        k = mat.spring_stiffness

        # displacement jump
        delta = Ue[3:] - Ue[:3]
        return k * delta

    def elementary_mass(self, mat, sect, lumped=False):
        """Elementary mass matrix"""
        m = mat.spring_mass
        # elementary mass matrix
        return np.kron(np.array(np.eye(2)), np.diag(m) / 2)

    def internal_forces_nl(self, DUe, Sige, Xe, mat, sect):
        """
        Stress update for nonlinear material constitutive law with new internal
        forces contribution, internal variable increments inside the element
        and elementary tangent stiffness matrix

        Parameters
        ----------
        DUe : ndarray
            increment of displacement shape=(4,)
        Sige : ndarray
            previous stresses in the element shape=(1,)
        Xe : ndarray
            previous internal variables in the element, shape depends on material

        Returns
        -------
        fint : ndarray
            elemental contribution to internal forces vector for new stress shape=(4,)
        sig : ndarray
            new stresses (axial force) shape=(1,)
        dXe : ndarray
            internal variable increment
        Kte : ndarray
            elementary tangent stiffness matrix shape=(4,4)
        """
        delta = DUe[3:] - DUe[:3]

        sig, dXe, Ct = mat.constitutive_relation(delta, Sige, Xe)

        fint = np.kron(np.array([-1, 1]), sig)

        # elementary tangent stiffness matrix
        Kte = np.kron(np.array([[1, -1], [-1, 1]]), np.diag(Ct))

        return fint, sig, dXe, Kte

    def internal_forces(self, Sige):

        fint = np.kron(np.array([-1, 1]), Sige)
        return fint