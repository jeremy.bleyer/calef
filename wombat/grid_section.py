# -*- coding: utf-8 -*-
"""
Additional functions for using `Grid3D` elements

This file is part of the **WomBat** finite element code
used for the *Civil Engineering Finite Element Course*
of *Ecole des Ponts ParisTech* 2017-2018

@author: Jeremy Bleyer, Ecole des Ponts ParisTech,
Laboratoire Navier (ENPC,IFSTTAR,CNRS UMR 8205)
@email: jeremy.bleyer@enpc.fr
"""
from wombat.geometric_caract import BeamSection


class GridSection(BeamSection):
    """Represents geometric properties of a bar/beam cross-section

    Attributes
    ----------
    area : float
        area :math:`S` of the cross-section
    inertia : float
        bending inertia :math:`I` for a torsional beam
    torsion : float
        torsional inertia :math:`J` for a torsional beam
    """

    def __init__(self, S=1.0, I=0, J=0):
        self.area = S
        self.inertia = I
        self.torsion = J
