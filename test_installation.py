# -*- coding: utf-8 -*-
import subprocess
import platform

def test_wombat():
    try:
        import wombat
    except:
        raise ImportError("wombat is not properly installed. You must execute 'setup.py'.")

def test_meshio():
    try:
        import meshio
    except:
        raise ImportError("meshio is not properly installed. You can get it from 'pip' or 'conda'.")

def test_gmsh():
    try:
        import wombat
        s = wombat.input.get_gmsh(["-help"])
    except:
        s = 1
    finally:
        if s != 0:
            raise ImportError("Gmsh is not available.")
            
if __name__ == '__main__':
    test_wombat()
    test_meshio()
    test_gmsh()
    print("\n")
    print("INSTALLATION SUCCESSFUL!")