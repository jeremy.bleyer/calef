// Number of elements across wall length
n = 2;
// Width
a = 20.;
// Total height
b1 = 10.;
// Wall height
b2 = 5.;
// Wall length
b3 = 7;
// Wall width
h = 0.2;
// Loading width
w = 1.5;
// Mesh density on external corners
d = 2.;
// Mesh density around the wall
d2 = b3/n;

Point(1) = {0, 0., 0, d};
Point(2) = {a, 0., 0, d};
Point(3) = {a, b1-b2, 0, d};
Point(4) = {a/2+h, b1-b2, 0, d2};
Point(5) = {a/2+h, b1, 0, d2};
Point(6) = {a/2, b1, 0, d2};
Point(7) = {a/2-w, b1, 0, d2};
Point(12) = {a/2-2*w, b1, 0, d2};
Point(8) = {0, b1, 0, d};
Point(9) = {a/2, b1-b2, 0, d2};
Point(10) = {a/2, b1-b3, 0, d2};
Point(11) = {a/2+h, b1-b3, 0, d2};

//+
Line(1) = {1, 1};
//+
Line(2) = {1, 2};
//+
Line(3) = {2, 3};
//+
Line(4) = {3, 4};
//+
Line(5) = {4, 5};
//+
Line(6) = {5, 6};
//+
Line(7) = {6, 7};
Line(15) = {7, 12};
//+
Line(8) = {12, 8};
//+
Line(9) = {8, 1};
//+
Line(10) = {6, 9};
//+
Line(11) = {9, 4};
//+
Line(12) = {9, 10};
//+
Line(13) = {10, 11};
//+
Line(14) = {11, 4};
//+
Curve Loop(1) = {2, 3, 4, -14, -13, -12, -10, 7, 15, 8, 9};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {13, 14, -11, 12};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {5, 6, 10, 11};
//+
Plane Surface(3) = {3};
//+
Transfinite Surface {2};
//+
Transfinite Surface {3};
//+
Physical Curve("sides") = {9, 3};
//+
Physical Curve("bottom") = {2};
//+
Physical Curve("top") = {15};
//+
Physical Curve("wall_face") = {14, 5};
//+
Physical Surface("wall") = {2, 3};
//+
Physical Surface("soil") = {1};
