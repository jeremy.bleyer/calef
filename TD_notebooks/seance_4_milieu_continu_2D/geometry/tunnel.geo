// Gmsh project created on Mon Sep 14 09:33:57 2015
Lx = 50.;
Ly = 100.;
H = 50.;
R = 3.;
e = 0.3;
d1 = 2;
d2 = 0.3;

Point(1) = {0, -Ly, 0, d1};
Point(2) = {Lx, -Ly, 0, d1};
Point(3) = {Lx, 0, 0, d1};
Point(4) = {0, 0, 0, d1};
Point(5) = {R,-H,0,d2};
Point(6) = {0,-H-R,0,d2};
Point(7) = {0,-H+R,0,d2};
Point(8) = {R+e,-H,0,d2};
Point(9) = {0,-H-R-e,0,d2};
Point(10) = {0,-H+R+e,0,d2};
Point(11) = {0,-H,0,d2};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 10};
Line(5) = {10, 7};
Line(6) = {6, 9};
Line(7) = {9, 1};
Circle(8) = {6, 11, 7};
Circle(9) = {9, 11, 10};

Line Loop(10) = {1, 2, 3, 4, -9, 7};
Plane Surface(1) = {10};
Line Loop(12) = {-8, 5, 9, 6};
Plane Surface(2) = {12};

Physical Line("bottom") = {1};
Physical Line("right") = {2};
Physical Line("top") = {3};
Physical Line("left") = {4,5,6,7};
Physical Line("intrados") = {8};
Physical Line("extrados") = {9};

//Physical Surface("domain") = {1,2};

Physical Surface("soil") = {1};
Physical Surface("reinforcement") = {2};

