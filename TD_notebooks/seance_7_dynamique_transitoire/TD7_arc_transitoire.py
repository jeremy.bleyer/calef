from wombat import *
from wombat.linear_algebra import compute_Rayleigh_damping
import math
import matplotlib.pyplot as plt
from matplotlib import animation

Nel = 100

R = 4
t = np.linspace(0, math.pi, 2 * Nel + 1)
n0 = Node2D([-R, 0])  # get first node
na = n0  # na is the current starting node of an element
el_list = []
for i in range(2 * Nel):
    nb = Node2D(
        [-R * math.cos(t[i + 1]), R * math.sin(t[i + 1])]
    )  # nb is the current end node
    el_list.append(Beam2D([na, nb]))  # add a new element to the list
    if i == Nel - 1:  # get mid node
        n1 = nb
    na = nb  # old end node becomes new starting node
n2 = nb  # get end node


mesh = Mesh(el_list)

sect = BeamSection().rect(0.1)
mat = LinearElastic(70e9, rho=2700.0)

w = mat.rho * sect.area * 9.81
self_weight = ExtForce()
self_weight.add_distributed_forces(mesh.elem_list, fy=-w)
lateral_acc = ExtForce()
lateral_acc.add_distributed_forces(mesh.elem_list, fx=w)
appuis = Connections()
appuis.add_imposed_displ([n0, n2], ux=0, uy=0, thetaz=0)

model = Model(mesh, mat, sect)


K = model.assembl_stiffness_matrix()
M = model.assembl_mass_matrix()
L, Ud = model.assembl_connections(appuis)
Fy = model.assembl_external_forces(self_weight)
Fx = model.assembl_external_forces(lateral_acc)

U0, lamb0 = model.solve(K, Fy, L, Ud)

C = compute_Rayleigh_damping(K, M, alpha_M=0.001, alpha_K=0.001)
V0 = 0 * U0

evol = np.loadtxt("acceleration.txt")[:5000]

time = np.linspace(0, 0.005 * len(evol), len(evol))
time_fine = np.linspace(0, time[-1], len(time))
evol_fine = np.interp(time_fine, time, evol)

T_accel_lin = TimeIntegrator(time_fine, evol_fine, gamma=0.5, beta=0.25)
U, V = T_accel_lin.solve_evolution(U0, V0, K, C, M, [Fx, Fy], L, Ud)


dx = n1.get_dof("ux")
dy = n1.get_dof("uy")

plt.figure(1)
plt.plot(time_fine, U[dx, :], "-b", label="horizontal motion", linewidth=0.5)
plt.ylabel("Horizontal displacement")
plt.xlabel("Time")
plt.show()

# fig = Figure(1)
# plt.ion()
# fig.show()
# fig = Figure(1)
fig = plt.figure()  # initialise la figure
plt.plot(mesh.coor[:, 0], mesh.coor[:, 1], "-k")
(line,) = plt.plot([], [])
plt.xlim(1.1 * min(mesh.coor[:, 0]), 1.1 * max(mesh.coor[:, 0]))
plt.ylim(0, 1.1 * max(mesh.coor[:, 1]))
plt.gca().set_aspect("equal")


def init():
    line.set_data([], [])
    return (line,)


def animate(i):
    ampl = 200.0
    line.set_data(
        mesh.coor[:, 0] + ampl * U[::3, i], mesh.coor[:, 1] + ampl * U[1::3, i]
    )
    return (line,)


ani = animation.FuncAnimation(
    fig,
    animate,
    init_func=init,
    frames=len(time_fine),
    blit=True,
    interval=10,
    repeat=False,
)
plt.show()
