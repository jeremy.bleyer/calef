"""Test node module."""
from wombat import Node2D, NodeGroup
import numpy as np


def test_node():
    """Test node creation."""
    coor = np.random.rand(2)
    n = Node2D(coor)
    assert np.all(n.coor == coor)
    assert n.dim == 2


def test_node_group():
    """Test node group creation."""
    coords = np.random.rand(5, 2)
    node_list = []
    for coor in coords:
        node_list.append(Node2D(coor))
    node_group = NodeGroup(node_list)
    assert node_group.get_pos(node_list[2]) == 2
