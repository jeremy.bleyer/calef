"""Test fields module."""
from wombat.fields import NodalField, ElementField
from wombat import NodeGroup, Mesh
from generate_composite_mesh import n0, n1, n2, el1, el3
import numpy as np


mesh = Mesh([el1, el3])


def test_nodal_field():
    """Test NodalField class."""
    values = np.random.rand(6)
    field = NodalField(NodeGroup([n0, n1, n2]), values)

    n0.get_dof("Ux")
    viewx = field.get("Ux")
    viewy = field.get("uy")

    assert all(viewx == values[::2])
    assert all(viewy == values[1::2])


def test_linear_algebra():
    values = np.random.rand(6)
    M = np.random.rand(6, 6)
    field = NodalField(NodeGroup([n0, n1, n2]), values)
    a1 = 2 * field
    assert np.allclose(a1._array, values * 2, 1e-6)
    assert field @ field == values @ values
    a2 = M @ field
    assert np.allclose(a2, M @ values, 1e-6)
    a3 = field @ M
    assert np.allclose(a3, values @ M, 1e-6)
    assert np.isclose(field @ M @ field, values @ M @ values)
    assert np.isclose(field @ (M @ field), values @ M @ values)
    assert np.isclose((field @ M) @ field, values @ M @ values)


def test_element_field():
    """Test ElementField class."""
    field = ElementField(mesh.elem_list, 5)
    field.update(mesh.elem_list[0], {"N": 0.1})
    field.update(mesh.elem_list[1], {"N": 0.2, "V": 0.3, "M": [1, -1]})
    assert field.subfield_names == set(["N", "M", "V"])
    assert np.allclose(field.get("N"), np.array([0.1, 0.2]))
    assert np.allclose(field.get("V"), np.array([0.0, 0.3]))
    assert np.allclose(field.get("M"), np.array([[0, 0], [1, -1]]))
