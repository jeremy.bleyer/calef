"""Test connection module."""
from wombat import (
    Mesh,
    Connections,
    ExtForce,
    Model,
    LinearElastic,
    BeamSection,
)
from generate_composite_mesh import n0, n1, n2, el1, el3
import numpy as np


mesh = Mesh([el1, el3])

mat = LinearElastic(1.0)
sect = BeamSection(1.0)
model = Model(mesh, mat, sect)


def test_connections():
    """Test Connections class."""
    appuis = Connections()  # création d'un objet Connections (CL, liaisons)
    appuis.add_imposed_displ([n0, n2], ux=[0, 0], uy=[0, None])
    appuis.add_relation(
        n0.get_dof("Ux"),
        [n1.get_dof("Ux"), n2.get_dof("Uy")],
        lin_relation=[1, -1, -2],
        imposed_value=0.5,
    )

    L, Ud = model.assembl_connections(appuis)  # assemblage des liaisons
    expected_L = np.array(
        [
            [1, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0],
            [0, 0, 0, 0, 1, 0],
            [1, 0, -1, 0, 0, -2],
        ],
        dtype=np.float64,
    )
    assert L.shape == (4, 6)
    for el in expected_L:
        assert any([all(el == line) for line in L.toarray()])


def test_forces():
    """Test forces class."""
    forces = ExtForce()
    forces.add_concentrated_forces(n1, Fy=-0.5)
    forces.add_distributed_forces(el1, fy=2.0, fx=3.0)

    F = model.assembl_external_forces(forces)
    np.allclose(F, np.array([3, 2, 3, 2 - 0.5, 0, 0]))


test_connections()