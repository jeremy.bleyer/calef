"""Test mesh module."""
from wombat import Mesh, Node2D

n_dummy = Node2D([0, 0])

from generate_composite_mesh import el1, el2, el3


def test_mesh():
    """Test mesh creation."""
    mesh = Mesh([el1, el3, el2])
    assert mesh.Ndof == 8
