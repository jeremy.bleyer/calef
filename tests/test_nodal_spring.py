# coding: utf-8
from wombat import (
    Node2D,
    NodalSpring,
    Beam2D,
    Mesh,
    BeamSection,
    LinearElastic,
    LinearSpring,
    Model,
    ExtForce,
    Connections,
    Figure,
)
from math import sqrt, pi

# longueur de la poutre
length = 1.0

# creation des noeuds
n1 = Node2D([-length, 0])
n2 = Node2D([0, 0])
n2c = n2.copy()
n4 = Node2D([length, 0])
# creation d’un element de type poutre
el1 = Beam2D([n1, n2])
el2 = Beam2D([n2c, n4])
el3 = NodalSpring([n2, n2c])
# maillage compose d’un seul element
mesh = Mesh([el1, el2, el3])

# creation d’une section carree de cote a=1
sect_beam = BeamSection().rect(1.0)
# affichage de la valeur de l’aire A=a**2 et de l’inertie I=a**4/12 de la section
# print "Area : %f Inertia : %f" % (sect.area,sect.inertia)
mat_beam = LinearElastic(E=1e3, rho=1.0)
# construction d'un modele (affectation de 'mat' et 'sect' a l'ensemble du maillage)
k_trans = 1e6
m_trans = 1e3
mat_spring = LinearSpring([k_trans, k_trans, 0.0], [m_trans, m_trans, 0])
model = Model(mesh, mat_beam, sect_beam)
model.affect_property(mat_spring, [el3])

forces = ExtForce()
forces.add_concentrated_forces(n2c, Fy=1)

appuis = Connections()
appuis.add_imposed_displ([n1, n4], ux=0, uy=0, thetaz=0)

for el in mesh.elem_list:
    print(el.get_dofs())

K = model.assembl_stiffness_matrix()  # assemblage de la rigidite
L, Ud = model.assembl_connections(appuis)  # assemblage des liaisons
F = model.assembl_external_forces(forces)  # assemblage des efforts exterieurs

U, lamb = model.solve(K, F, L, Ud)  # resolution du systeme

print(U.get("Ux"))
print(U.get("Uy"))
print(U.get("Thetaz"))


fig = Figure(1)  # creation d'une figure
fig.plot(mesh)  # dessin du maillage
fig.plot_def(mesh, U, 100)  # dessin de la deformee (amplitude x20)
fig.show()  # commande d'affichage


Sig = model.stresses(U)

N = Sig.get("N")
V = Sig.get("V")
M = Sig.get("M")

fig = Figure(3, "Normal force")
fig.plot(mesh)
fig.plot_field_diagrams(mesh, N, 0.2)
fig.show(block=False)
fig = Figure(4, "Shear force")
fig.plot(mesh)
fig.plot_field_diagrams(mesh, V, 0.2)
fig.show(block=False)
fig = Figure(5, "Bending moment")
fig.plot(mesh)
fig.plot_field_diagrams(mesh, M, 0.2)
fig.show()


M = model.assembl_mass_matrix()


Nmodes = 4
omega2, xi = model.eigenmodes(K, M, L, Nmodes)

fig = Figure(2, "Eigenmodes", [Nmodes, 1])
for i in range(Nmodes):
    f_lumped = sqrt(omega2[i]) / 2 / pi
    fig.add_subplot(i)
    fig.plot_def(mesh, xi[i], 10)
fig.show()