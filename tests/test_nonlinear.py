#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# -*- coding: utf-8 -*-
from wombat import (
    Node2D,
    Bar2D,
    Mesh,
    BeamSection,
    Cable,
    ElastoPlasticBar,
    Model,
    Connections,
)
import numpy as np
import matplotlib.pyplot as plt

n1 = Node2D([0, 0])
n2 = Node2D([0, 1])
el = Bar2D([n1, n2])
mesh = Mesh([el])

sect = BeamSection(1.0)
mat = Cable(E=1e4)
mat = ElastoPlasticBar(E=1e4, fy=5e3)
# mat = LinearElastic(E=1e4)

model = Model(mesh, mat, sect)

appuis = Connections()
appuis.add_imposed_displ([n1, n2], ux=0, uy=[0, 1.0])

L, Ud = model.assembl_connections(appuis)

Nsteps = 20
Fmax = -1.0
Ffin = -Fmax
if Ffin == Fmax:
    ampl = np.linspace(0, Fmax, Nsteps + 1)
else:
    ampl = np.hstack(
        (0, np.linspace(0, Fmax, Nsteps + 1), np.linspace(Fmax, Ffin, Nsteps + 1)[1:])
    )

X = np.array([0.5])
U, lamb, Sig, X, info = model.nonlinear_solver(
    ampl,
    0,
    L,
    Ud,
    X=X,
    method="newton-raphson",
    print_info=True,
    nitermax=20,
    tol=1e-6,
)
fNsteps = len(Sig)
u = np.zeros((fNsteps,))
N = np.zeros((fNsteps,))
for i in range(fNsteps):
    u[i] = U[3, i]
    N[i] = Sig[i].get("N")

plt.plot(u[:Nsteps], N[:Nsteps], "xr")
plt.plot(u[Nsteps:], N[Nsteps:], "sb")
plt.show()