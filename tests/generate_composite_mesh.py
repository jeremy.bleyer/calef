"""Helper file generating data for tests."""
from wombat import Node2D, Segment, Segment3
import numpy as np


class MyBar(Segment):
    """Custom bar element."""

    def __init__(self, node_list, tag=0):
        Segment.__init__(self, node_list, tag)
        self.dof_names = ["Ux", "Uy"]
        self.force_names = ["Fx", "Fy"]

    def elementary_distributed_forces(self, f):  # noqan
        return np.array([f[0], f[1], f[0], f[1]])


class MyBar3(Segment3):
    """Custom bar3 element."""

    def __init__(self, node_list, tag=0):
        Segment3.__init__(self, node_list, tag)
        self.dof_names = ["Ux", "Uy"]
        self.force_names = ["Fx", "Fy"]

    def elementary_distributed_forces(self, f):  # noqa
        return np.array([f[0], f[1], f[0], f[1]])


n0 = Node2D([0.0, 0.0])
n1 = Node2D([1.0, 0.0])
n2 = Node2D([0.0, 1.0])
ninter = Node2D([0.5, 0.5])
el1 = MyBar([n0, n1])
el2 = MyBar3([n1, n2, ninter])
el3 = MyBar([n0, n2])
