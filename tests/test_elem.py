"""Test generic element functionality."""
from wombat import ElementGroup
from generate_composite_mesh import n0, n1, n2, ninter, el1, el2, el3


def test_dofs():
    """Test dofs generation."""
    mesh = ElementGroup([el1, el3, el2])
    mesh._affect_dofs()
    mesh._generate_dofmap()
    d0 = list(n0._dofs.values())
    d1 = list(n1._dofs.values())
    d2 = list(n2._dofs.values())
    dinter = list(ninter._dofs.values())
    assert d0[0] == n0.get_dof("Ux")
    assert d0[0] == n0.get_dof("ux")
    assert d0[1] == n0.get_dof("Uy")
    assert all(d0 == n0.get_dofs())
    assert all(el1.get_dofs() == d0 + d1)
    assert all(el2.get_dofs() == d1 + d2 + dinter)
    assert all(el3.get_dofs() == d0 + d2)
    assert n0._force_to_dof_name == {"fx": "ux", "fy": "uy"}
